package com.example.invtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

//@SpringBootApplication
//public class InvtestApplication {
//
//	public static void main(String[] args) {
//		SpringApplication.run(InvtestApplication.class, args);
//	}
//
//}


@SpringBootApplication
public class InvtestApplication extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(InvtestApplication.class);
	}



	public static void main(String[] args) {
		SpringApplication.run(InvtestApplication.class, args);
	}
}

